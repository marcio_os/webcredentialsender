package br.ind.datacom.vaadin.webcredentialsender;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Testes para {@link WebCredentialSender}.
 *
 * @author marcio
 *
 */
@RunWith(JUnit4.class)
public class WebCredentialSenderTest {

    private static final String VALID_URL = "https://localhost"; //$NON-NLS-1$
    private static final String VALID_USERNAME = "user"; //$NON-NLS-1$
    private static final String VALID_PASSWORD = "passwd"; //$NON-NLS-1$
    private static final String EMPTY_STRING = ""; //$NON-NLS-1$
    private static final String INVALID_URL = "http://localhost"; //$NON-NLS-1$

    @Test
    public void shouldBuildSenderNormallyWithProperArguments() {
        // given
        String url = VALID_URL;
        String username = VALID_USERNAME;
        String password = VALID_PASSWORD;

        // when
        new WebCredentialSender(url, username, password);

        // then no exceptions should have been thrown.
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenNullUsernameIsPassed() {
        // given
        String url = VALID_URL;
        String username = null;
        String password = VALID_PASSWORD;

        // when
        new WebCredentialSender(url, username, password);

        // then should have thrown IllegalArgumentException.
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenNullPasswordIsPassed() {
        // given
        String url = VALID_URL;
        String username = VALID_USERNAME;
        String password = null;

        // when
        new WebCredentialSender(url, username, password);

        // then should have thrown IllegalArgumentException.
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenNullURLIsPassed() {
        // given
        String url = null;
        String username = VALID_USERNAME;
        String password = VALID_PASSWORD;

        // when
        new WebCredentialSender(url, username, password);

        // then should have thrown IllegalArgumentException.
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenURLIsNotHTTPS() {
        // given
        String url = INVALID_URL;
        String username = VALID_USERNAME;
        String password = VALID_PASSWORD;

        // when
        new WebCredentialSender(url, username, password);

        // then should have thrown IllegalArgumentException.
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenUsernameIsEmpty() {
        // given
        String url = VALID_URL;
        String username = EMPTY_STRING;
        String password = VALID_PASSWORD;

        // when
        new WebCredentialSender(url, username, password);

        // then should have thrown IllegalArgumentException.
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenPasswordIsEmpty() {
        // given
        String url = VALID_URL;
        String username = VALID_USERNAME;
        String password = EMPTY_STRING;

        // when
        new WebCredentialSender(url, username, password);

        // then should have thrown IllegalArgumentException.
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenURLIsEmpty() {
        // given
        String url = EMPTY_STRING;
        String username = VALID_USERNAME;
        String password = VALID_PASSWORD;

        // when
        new WebCredentialSender(url, username, password);

        // then should have thrown IllegalArgumentException.
    }
}
