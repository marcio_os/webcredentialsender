package br.ind.datacom.vaadin.webcredentialsender;

import org.vaadin.risto.formsender.FormSender;
import org.vaadin.risto.formsender.FormSender.Method;

import br.ind.datacom.vaadin.webcredentialsender.gwt.client.VWebCredentialSender;

import com.vaadin.terminal.PaintException;
import com.vaadin.terminal.PaintTarget;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.ClientWidget;

/**
 * Envia credenciais (usuário e senha) via HTTP (método POST) para uma url.
 *
 * @author marcio
 */
@ClientWidget(VWebCredentialSender.class)
public class WebCredentialSender extends AbstractComponent {

    private static final String PASSWORD_KEY = "p"; //$NON-NLS-1$
    private static final String USERNAME_KEY = "u"; //$NON-NLS-1$
    private static final String HTTPS = "https"; //$NON-NLS-1$

    private final FormSender formsender;

    /**
     * Joga {@link IllegalArgumentException} caso a url não seja HTTPS ou se qualquer argumento for {@code null} ou vazio.
     */
    public WebCredentialSender(String url, String username, String password) {
        validateInput(url, username, password);
        this.formsender = new FormSender(Method.POST);
        setImmediate(true);
        setFormSenderParameters(url, username, password);
    }

    public void submit() {
        formsender.submit();
        requestRepaint();
    }

    @Override
    public void paintContent(PaintTarget target) throws PaintException {
        super.paintContent(target);
        formsender.paintContent(target);
    }

    private void setFormSenderParameters(String url, String username, String password) {
        formsender.setFormTarget(url);
        formsender.addValue(USERNAME_KEY, username);
        formsender.addValue(PASSWORD_KEY, password);
    }

    private void validateInput(String url, String username, String password) {
        URLValidator.INSTANCE.validate(url);
        NonNullableStringValidator.INSTANCE.validate(username);
        NonNullableStringValidator.INSTANCE.validate(password);
    }

    /**
     * Valida a url utilizada para criar um {@link WebCredentialSender}.
     *
     * @author marcio
     *
     */
    private static enum URLValidator {
        INSTANCE;

        /**
         * Valida uma URL, jogando {@link IllegalArgumentException} caso a mesma seja vazia, nula, ou não seja https.
         *
         * @param url
         *            URL que será validada.
         * @throws IllegalArgumentException
         *             Caso a URL seja nula, vazia ou não seja https.
         */
        public void validate(String url) {
            NonNullableStringValidator.INSTANCE.validate(url);
            if (!url.contains(HTTPS)) {
                throw new IllegalArgumentException("Use HTTPS protocol."); //$NON-NLS-1$
            }
        }
    }

    /**
     * Valida uma {@link String} que não pode ser nula ou vazia.
     *
     * @author marcio
     *
     */
    private static enum NonNullableStringValidator {
        INSTANCE;

        /**
         * Valida a {@link String} passada, jogando {@link IllegalArgumentException} caso a mesma seja vazia ou nula.
         *
         * @param string
         *            {@link String} que será validada.
         * @throws IllegalArgumentException
         *             Caso a {@link String} seja nula ou vazia.
         */
        public void validate(String string) {
            if (string == null || string.isEmpty()) {
                throw new IllegalArgumentException("Parameter cannot be null or empty"); //$NON-NLS-1$
            }
        }
    }
}
