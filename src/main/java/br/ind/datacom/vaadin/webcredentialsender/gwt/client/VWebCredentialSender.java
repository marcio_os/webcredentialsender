package br.ind.datacom.vaadin.webcredentialsender.gwt.client;

import org.vaadin.risto.formsender.widgetset.client.ui.VFormSender;

import br.ind.datacom.vaadin.webcredentialsender.WebCredentialSender;

import com.vaadin.terminal.gwt.client.Paintable;

/**
 * Componente client-side para o {@link WebCredentialSender}.
 *
 * @author marcio
 *
 */
public class VWebCredentialSender extends VFormSender implements Paintable {

}
